{
  const {
    html,
  } = Polymer;
  /**
    `<cells-luis-kelly-header>` Description.

    Example:

    ```html
    <cells-luis-kelly-header></cells-luis-kelly-header>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-luis-kelly-header | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsLuisKellyHeader extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-luis-kelly-header';
    }

    static get properties() {
      return {};
    }

    static get template() {
      return html `
      <style include="cells-luis-kelly-header-styles cells-luis-kelly-header-shared-styles"></style>
      <slot></slot>
      
      <nav>
          <div class="vertical-align-middle">

              <div class="corner-item left-corner">
                  <div class="hamburger-menu">
                          <span class="menu-line"></span>
                          <span class="menu-line"></span>
                          <span class="menu-line"></span>
                  </div>
              </div>

              <div class="middle-item">
                  <h1>header-component-title</h1>
              </div>

              <div class="corner-item right-corner" style="text-align: right;padding-top:15px;">
                  <span class="help-icon">?</span>
              </div>

          </div>
      </nav>
      
      `;
    }
  }

  customElements.define(CellsLuisKellyHeader.is, CellsLuisKellyHeader);
}